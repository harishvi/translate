package tychestudios.translate;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends Activity implements
        TextToSpeech.OnInitListener {

    private TextView txtSpeechInput;
    private CoordinatorLayout coordinatorLayout;
    private ImageButton btnSpeak;
    private Button button;
    private AutoCompleteTextView source_lang, target_lang;
    private TextToSpeech tts;
    public String sourceText = null;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private String sourceLanguage="hi", targetLanguage="en-in";
    public static String[] Languages = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .snackbarPosition);

        Languages = (String[]) getIntent().getSerializableExtra("Languages");
        Log.i("Translate: ","Language "+Languages);
        source_lang = (AutoCompleteTextView) findViewById(R.id.sourceLang);
        target_lang = (AutoCompleteTextView) findViewById(R.id.targetLang);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, Languages);
        source_lang.setAdapter(adapter);
        target_lang.setAdapter(adapter);

        txtSpeechInput = (TextView) findViewById(R.id.txtSpeechInput);
        btnSpeak = (ImageButton) findViewById(R.id.btnSpeak);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speakOut(translatedText);
            }
        });
        button.setVisibility(View.INVISIBLE);
        tts = new TextToSpeech(this, this);

        // hide the action bar
        //getActionBar().hide();
        btnSpeak.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!source_lang.getText().toString().trim().equals("") && !target_lang.getText().toString().trim().equals("")) {
                    setTranslate(source_lang.getText().toString().trim(), target_lang.getText().toString().trim());
                    Log.i("Debug:Translate", "Snackbar should not show itself.");
                    button.setVisibility(View.INVISIBLE);
                    promptSpeechInput();
                } else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Language not selected. Default is English to Hindi.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    Log.i("Debug:Translate", "Snackbar should show itself.");
                    button.setVisibility(View.INVISIBLE);
                    promptSpeechInput();
                }
            }
        });
    }


    private void setTranslate(String source, String target) {
        sourceLanguage = source;
        targetLanguage = target;
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    txtSpeechInput.setText(result.get(0));
                    if (result.get(0) != null) {
                        Log.i("Speech", result.get(0));
                        convertToHindi(result.get(0));
                    }
                }
                break;
            }

        }
    }

    String translatedText = null;

    private void convertToHindi(String sourceText2) {
        sourceText = sourceText2;
        new MyAsyncTask() {
            protected void onPostExecute(Boolean result) {
                speakOut(translatedText);
            }
        }.execute();
    }

    class MyAsyncTask extends AsyncTask<Void, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(Void... arg0) {
            Language SOURCE_LANG = null ,TARGET_LANG = null;
            Translate.setClientId("harishvi");
            Translate.setClientSecret("mfVZZk+kIGEhQR1E8xzBQzHr3ie/7cK+nC6uo31WJcQ=");
            try {

                    if (!Language.getLanguageCodesForTranslation().contains(sourceLanguage)) {
                        SOURCE_LANG = Language.ENGLISH;
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "Source language not supported. Default is English.", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if(!Language.getLanguageCodesForTranslation().contains(targetLanguage)){
                        TARGET_LANG = Language.HINDI;
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "Target language not supported. Default is Hindi.", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    else
                {
                        SOURCE_LANG = Language.fromString(sourceLanguage);
                    TARGET_LANG=Language.fromString(targetLanguage);
                    }

                translatedText = Translate.execute(sourceText, SOURCE_LANG, TARGET_LANG);
            } catch (Exception e) {
                translatedText = e.toString();
                e.printStackTrace();
                Log.i("Translate: ","translated text "+translatedText+" "+sourceLanguage+" "+targetLanguage);

            }
            return true;
        }
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }*/

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = tts.setLanguage(new Locale(sourceLanguage));
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                btnSpeak.setEnabled(true);
                speakOut("Hello");
            }
        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }

    private void speakOut(String text) {
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, "1");
        button.setVisibility(View.VISIBLE);
    }



}
